package com.nvt.interview.model;

public class Car {

    private final Long id;

    private final String brand;

    private final Long storeId;

    public Car(Long id, String brand, Long storeId) {
        this.id = id;
        this.brand = brand;
        this.storeId = storeId;
    }

    public Long getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public Long getStoreId() {
        return storeId;
    }

}
