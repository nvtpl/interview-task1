package com.nvt.interview;

import com.nvt.interview.dto.StoreStock;
import com.nvt.interview.repository.CarRepository;
import com.nvt.interview.repository.StoreRepository;

import java.util.ArrayList;
import java.util.List;

public class StockService {

    private final CarRepository carRepository;
    private final StoreRepository storeRepository;

    public StockService(CarRepository carRepository, StoreRepository storeRepository) {
        this.carRepository = carRepository;
        this.storeRepository = storeRepository;
    }

    public List<StoreStock> getStoreStock() {

        return new ArrayList<>();
    }
}
